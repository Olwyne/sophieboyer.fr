<!DOCTYPE HTML>
<html lang="fr" >

<head>
        <?php
		  require_once('./assets/thumbnail/header.php');
		  generate_head();
 		?>
</head>

<body class="homepage  Accueilpage">
    <div id="page-wrapper">
        <!-- Header -->
        <div id="header">
            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1 id="logo">Portfolio</h1>
                    <hr />
                    <?php
						if (!isset($_SESSION)) { session_start(); }
						if($_SESSION["language"]=='en-US'){
							$html = <<<HTML
								<p>My projects</p>
HTML;
							echo $html;
							}
					  
						elseif($_SESSION["language"]=='fr-FR'){
							$html = <<<HTML
								<p>Mes réalisations</p>
HTML;
							echo $html;
                        }

					  
					?>
                    
                </header>
                <footer>
                    <a href="#go" class="button circled scrolly icon fas fa-arrow-down"></a>
                </footer>
            </div>
            <!-- Nav -->
           <?php
                require_once('./assets/thumbnail/menu.php');
                generate_menu();
            ?>  
        </div>
        <div id="go" class="wrapper style1">
            <button onclick="topFunction()" id="myBtn" title="Go to top" class="icon fas fa-arrow-up"></button>

            <section class="container special">
                 <header>
                 <?php
                 if (!isset($_SESSION)) { session_start(); }
						if($_SESSION["language"]=='en-US'){
						$html = <<<HTML
                            <span id="all" class="btnmenu">All</span>
                            <span  id="web" class="btnmenu">Web and application</span>
                            <span id="graphisme" class="btnmenu">Design</span>
                            <span id="audiovisuel" class="btnmenu">Audiovisual</span>
                            <span id="jeux vidéo" class="btnmenu">Video games and 3D</span>
HTML;
							echo $html;
							}
					  
						elseif($_SESSION["language"]=='fr-FR'){
							$html = <<<HTML
								<span id="all"  class="btnmenu">Tout</span>
                                <span  id="web" class="btnmenu">Web et application</span>
                                <span id="graphisme" class="btnmenu">Graphisme</span>
                                <span id="audiovisuel" class="btnmenu">Audiovisuel</span>
                                <span id="jeux vidéo" class="btnmenu">Jeux vidéo et 3D</span>
HTML;
							echo $html;
						}
					  
					?>
                    
                    <!--  <span id="communication" class="btnmenu">Communication</span> -->
                </header>
                <div id="projects"  class="row">

                </div> 
             
            </section>


           
        </div>
        <!-- Footer -->
        <?php
                require_once('./assets/thumbnail/footer.php');
                generate_footer();
        ?>  
    </div>
    <!-- Scripts -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.dropotron.min.js"></script>
    <script src="assets/js/jquery.scrolly.min.js"></script>
    <script src="assets/js/jquery.onvisible.min.js"></script>
    <script src="assets/js/skel.min.js"></script>
    <script src="assets/js/util.js"></script>
    <!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
    <script src="assets/js/main.js"></script>
    <script src="assets/js/gallery.js"></script>
    <script src="assets/js/button.js"></script>
    
</body>

</html>
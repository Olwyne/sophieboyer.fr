<?php
/*
Copyright (C) 2017  Minute - CheckRaise
 
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
 
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/*
Plugin Name: WoWHead Tooltips
Plugin URI: Wordpress plugin directory.
Description: Adds tooltips for WoW items using WoWHead tooltips.
Author: Minute - Check Raise
Author URI: http://checkraise.fr
Version: 2.0.2
*/

function wowhead_tooltips_header() {
	echo '<!-- Wowhead Tooltips -->';
	echo '<script>var whTooltips = {colorLinks: true, iconizeLinks: true, renameLinks: true};</script>';
	echo '<script src="//wow.zamimg.com/widgets/power.js"></script>';
	echo '<!-- Wowhead Tooltips END -->';
}

add_action('wp_head','wowhead_tooltips_header', 20);


function wowhead_tooltips_shortcode($atts, $content) {
	return '<a href="//fr.wowhead.com/item='.$atts['id'].'" rel="item='.$atts['id'].'">item</a>';
}

add_shortcode('wowhead', 'wowhead_tooltips_shortcode');


function wowhead_tooltips_shortcode_alt($atts, $content) {
	return '<a href="//fr.wowhead.com/item='.$content.'" rel="item='.$content.'">item</a>';
}

add_shortcode('wow', 'wowhead_tooltips_shortcode_alt');

?>

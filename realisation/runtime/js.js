// JavaScript Document
window.onload = function() {
    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext('2d');
    var clavier = new Clavier();
    var fond5 = new Image();
    var fond4 = new Image();
    var fond3 = new Image();
    var fond2 = new Image();
    var fond1 = new Image();
    var perso = new Image();
    var bravo = new Image();
    var end = new Image();
    var go = new Image();
    var p0 = new Image();
    var p1 = new Image();
    var p2 = new Image();
    var p3 = new Image();
    var p4 = new Image();
    var p5 = new Image();
    var p6 = new Image();
    var p7 = new Image();
    var p8 = new Image();
    var p9 = new Image();
    var p10 = new Image();
    var m1 = new Image();
    var m2 = new Image();
    var m3 = new Image();
    var m4 = new Image();
    var att = new Image();
    var droite = 1;
    var posX = 0,
        posY = 265;
    var flip = false;
    var compteur = -1;
    var pas = 0;
    var s = 1;
    var dir = 2;
    var coll = false;
    var posInit = 265;
    var defil = 0;
    var sens = 1;
    var item1 = new Image();
    var x = 1;
    var ramasser = false;
    var fin = 0;
    var debut = 0;
    var y = 1;
    var wait = 0;
    var AmbiantSon = new Audio("son/rayman.mp3");
    var son1= new Audio ("son/piece.wav");
    var son2= new Audio ("son/saut.wav");
    var son3= new Audio ("son/gagner.mp3");
    var son4= new Audio ("son/hurt.wav");
	AmbiantSon.play();
    
    
// Chargement de l'image
    bravo.src = 'images/jour/fin.png';
    go.src = 'images/jour/go.png';
    fond5.src = 'images/jour/jour5.png';
    fond4.src = 'images/jour/jour4.png';
    fond3.src = 'images/jour/jour3.png';
    fond2.src = 'images/jour/jour2.png';
    fond1.src = 'images/jour/jour1.png';
    item1.src = 'images/item/coin.png';
    perso.src = 'images/perso1/run.png'; // Path par rapport au document HTML, pas par rapport au document JS
    p0.src = 'images/0.png';
    p1.src = 'images/1.png';
    p2.src = 'images/2.png';
    p3.src = 'images/3.png';
    p4.src = 'images/4.png';
    p5.src = 'images/5.png';
    p6.src = 'images/6.png';
    p7.src = 'images/7.png';
    p8.src = 'images/8.png';
    p9.src = 'images/9.png';
    p10.src = 'images/10.png';
    m1.src = 'images/pers03/idle.png';
    m2.src = 'images/pers03/idle.png';
    m3.src = 'images/pers03/idle.png';
    m4.src = 'images/pers03/idle.png';
    end.src = 'images/end.png';
    att.src = 'images/att.png';

//Obstacles
    var bordure2 = new Obstacle(0, 0, 0, 0);
    var bordure1 = new Obstacle(2200, 0, 230, 400);
    var piece1 = new Obstacle(550, 265, 100, 56);
    var piece2 = new Obstacle(1100, 220, 100, 56);
    var piece3 = new Obstacle(1400, 265, 100, 30);
    var piece4 = new Obstacle(1800, 220, 100, 30);
    var items = [piece1, piece2, piece3];
    var bordures = [bordure1, bordure2];
    var monstres = [monstre1, monstre2, monstre3, monstre4];
    var monstre1 = new Obstacle(700, 200, 70, 1);
    var monstre2 = new Obstacle(1200, 200, 70, 1);
    var monstre3 = new Obstacle(1600, 200, 70, 1);
    var monstre4 = new Obstacle(1950, 200, 70, 1);

// Une fois l'image chargée on déclenche la boucle
    fond4.onload = function() {

        setInterval(boucle, 50);

    };
 // Boucle
    function boucle() {


        console.log(posY);
        ctx.save();
        ctx.translate(0, 0);
        ctx.drawImage(fond5, -posX * 0.1 - 100, 0, 2844, 400);
        ctx.drawImage(fond4, -posX * 0.1 - 100, 0, 2844, 400);
        ctx.drawImage(fond3, -posX * 0.4 - 100, 0, 2844, 400);
        ctx.drawImage(fond2, -posX * 0.8 - 100, 0, 2844, 400);
        ctx.drawImage(fond1, -posX - 100, 0, 2844, 400);


        ctx.restore();
        ctx.save();



        ctx.translate(-posX, 0);
        ctx.drawImage(m1, y * 850, 0, 850, 480, 700, 280, 100, 50);
        ctx.drawImage(m2, y * 850, 0, 850, 480, 1200, 280, 100, 50);
        ctx.drawImage(m3, y * 850, 0, 850, 480, 1600, 280, 100, 50);
        ctx.drawImage(m4, y * 850, 0, 850, 480, 1950, 280, 100, 50);
        ctx.drawImage(end, 0, 0, 100, 100, 2300, 250, 100, 100);
      
//Item     
        if (!piece1.ramasser) {

            ctx.drawImage(item1, x * 100, 0, 100, 56, 550, 265, 100, 56);
        }
        if (!piece2.ramasser) {

            ctx.drawImage(item1, x * 100, 0, 100, 56, 1100, 220, 100, 56);
        }
        if (!piece3.ramasser) {

            ctx.drawImage(item1, x * 100, 0, 100, 56, 1400, 260, 100, 56);
        }
        if (!piece4.ramasser) {

            ctx.drawImage(item1, x * 100, 0, 100, 56, 1800, 220, 100, 56);
        }

        ctx.restore();


//Personnages
        ctx.save();
        ctx.translate(330, posY);
        ctx.scale(s, s);
        if (flip == true) {
            ctx.scale(-1, 1);
        }
        if (clavier.gauche || clavier.droite) {
            ctx.drawImage(perso, pas * 854, 0, 854, 480, -50, 0, 100, 65);


        } else if (!clavier.gauche || !clavier.droite) {
            pas = 5
            ctx.drawImage(perso, pas * 854, 0, 854, 480, -50, 0, 100, 65);

        }
        ctx.restore();

//Fin
        console.log(y);
        if (fin == 4 && posX >= 2000) {
            ctx.drawImage(bravo, 0, 0, 600, 400);
            son3.play();
        }
        if (fin == 0) {
            ctx.drawImage(p0, 10, 10, 150, 54);
        }
        if (fin == 1) {
            ctx.drawImage(p1, 10, 10, 150, 54);
        }
        if (fin == 2) {
            ctx.drawImage(p2, 10, 10, 150, 54);
        }
        if (fin == 3) {
            ctx.drawImage(p3, 10, 10, 150, 54);
        }
        if (fin == 4) {
            ctx.drawImage(p4, 10, 10, 150, 54);
        }
        if (debut <= 80) {
            ctx.drawImage(go, 0, 0, 600, 400);
            debut++;

        }
        
//Saut
        if (compteur >= 0) {
            posY = posInit - 80 + (compteur - 20) * (compteur - 20) * (compteur - 20) * (compteur - 20) / 2000;
            compteur--;
            if (flip == false)
                posX++;
            else
                posX--;
        }
        
//Animation item
        x++;
        y++;
        
        if (x >= 10) {
            x = 1;
        }
        if (x < 1) {
            x--;
        }
        if (y > 1) {
            y = 0;
        }


        if (pas >= 4) {
            sens = -1;
        }
        if (pas < 1) {
            sens = 1;
        }


//Commandes
        if (clavier.droite) {

            for (p in items) {
                if (piece1.collision(posX + 180 + 5, posY, 100, 65) && !piece1.ramasser) {

                    piece1.ramasser = true;
                    fin++;
                    son1.play();

                }

                if (piece2.collision(posX + 180 + 5, posY, 100, 65) && !piece2.ramasser) {

                    piece2.ramasser = true;
                    fin++;
                    son1.play();
                }

                if (piece3.collision(posX + 180 + 5, posY, 100, 65) && !piece3.ramasser) {

                    piece3.ramasser = true;
                    fin++;
                    son1.play();
                }
                if (piece4.collision(posX + 180 + 5, posY, 100, 65) && !piece3.ramasser) {

                    piece3.ramasser = true;
                    fin++;
                    son1.play();
                }
            }

            for (p in monstres) {
                if (monstre1.collision(posX + 200 + 5, posY - 50, 100, 1)) {

                    coll = true;
                    



                }
                if (monstre2.collision(posX + 200 + 5, posY - 50, 100, 1)) {

                    coll = true;



                }
                if (monstre3.collision(posX + 200 + 5, posY - 50, 100, 1)) {

                    coll = true;



                }
                if (monstre4.collision(posX + 200 + 5, posY - 50, 100, 1)) {

                    coll = true;



                }

            }
            for (p in bordures) {
                if (bordure1.collision(posX + 5 + 180, posY, 100, 65)) {
                    coll = true;

                }
                if (bordure2.collision(posX - 5 + 180, 0, 100, 65)) {
                    coll = true;

                }
            }


            if (!coll) {
                posX += 5;
                coll = false;
                flip = false;
                pas += sens;
                dir = 2;
            }
            if (coll == true) {


                son4.play();
                posX = 330;
                coll = false;

            }
        } else if (clavier.gauche) {
            for (p in monstres) {
                if (monstre1.collision(posX + 250, posY - 50, 100, 1)) {

                    coll = true;

                }
                if (monstre2.collision(posX + 250, posY - 50, 100, 1)) {

                    coll = true;
                }
                if (monstre3.collision(posX + 250, posY, 100, 1)) {

                    coll = true;
                }
                if (monstre4.collision(posX + 250, posY - 50, 100, 1)) {

                    coll = true;
                }


            }
            for (p in items) {
                if (piece1.collision(posX - 5 + 180, posY + 5, 100, 65) && !piece1.ramasser) {
                    fin++;
                    piece1.ramasser = true;
                    son1.play();
                }
                if (piece2.collision(posX - 5 + 180, posY + 5, 100, 65) && !piece2.ramasser) {
                    fin++;
                    piece2.ramasser = true;
                    son1.play();
                }
                if (piece3.collision(posX - 5 + 180, posY + 5, 100, 65) && !piece3.ramasser) {
                    fin++;
                    piece3.ramasser = true;
                    son1.play();
                }
                if (piece4.collision(posX + 180 + 5, posY, 100, 65) && !piece4.ramasser) {

                    piece4.ramasser = true;
                    fin++;
                    son1.play();
                }
            }
            for (p in bordures) {
                if (bordure1.collision(posX - 5 + 180, 0, 100, 65)) {
                    coll = true;

                }
                if (bordure2.collision(posX - 5 + 180, 0, 100, 65)) {
                    coll = true;

                }
            }
            if (!coll) {
                posX -= 5;
                coll = false;
                flip = true;
                pas += sens;
                dir = 2;
            }
            if (coll == true) {
                posX = 330;
                coll = false;
                son4.play();

            }

        } else if (clavier.espace) {

            
            if (compteur <= 0) {
                compteur = 40;
                posInit = posY;
            }
            for (p in items) {
                if (piece1.collision(posX + 180 + 5, posY + 5, 100, 65) && !piece1.ramasser) {
                    fin++;
                    piece1.ramasser = true;
                    son1.play();
                }
                if (piece2.collision(posX + 180 + 5, posY + 5, 100, 65) && !piece2.ramasser) {
                    fin++;
                    piece2.ramasser = true;
                    son1.play();
                }
                if (piece3.collision(posX + 180 + 5, posY + 5, 100, 65) && !piece3.ramasser) {
                    fin++;
                    piece3.ramasser = true;
                    son1.play();
                }
                if (piece4.collision(posX + 180 + 5, posY, 100, 65) && !piece4.ramasser) {

                    piece4.ramasser = true;
                    fin++;
                    son1.play();
                }

            }

            if (coll == true) {
                posX = 330;
                coll = false;
                son4.play();

            }
        }
    }
}
<!DOCTYPE HTML>
<html>
	<head>
		<?php
		  require_once('./assets/thumbnail/header.php');
		  generate_head();
 		?>
	</head>
	<body class="homepage Accueilpage" >
		<div id="page-wrapper">
			<!-- Header -->
			<div id="header">
			<!-- Inner -->
			<div class="inner">
				<header>
					<h1 id="logo">Sophie Boyer</h1>
					<hr />
					 <?php
					 if (!isset($_SESSION)) { session_start(); }
						if($_SESSION["language"]=='en-US'){
							$html = <<<HTML
								<p>Web Developper<br> Multimedia Engineer Student</p>
HTML;
							echo $html;
							}
					  
						elseif($_SESSION["language"]=='fr-FR'){
							$html = <<<HTML
								<p>Web Developpeuse<br> Étudiante ingénieure multimédia</p>
HTML;
							echo $html;
						}
					  
					?> 
				</header>
			</div>
			</div>

			<?php
		  		require_once('./assets/thumbnail/menu.php');
		  		generate_menu();
 			?>	
			<!-- Banner -->
			<!-- Footer -->
			<?php
		  		require_once('./assets/thumbnail/footer.php');
		  		generate_footer();
 			?>	
		</div>
		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.onvisible.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
			
	</body>
</html>
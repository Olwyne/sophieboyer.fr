<!DOCTYPE HTML>
<html>
	<head>
		<?php
		  require_once('./assets/thumbnail/header.php');
		  generate_head();
 		?>
	</head>
	<body class="homepage  Accueilpage">
		<div id="page-wrapper">
			<!-- Header -->
				<div id="header">
					<!-- Inner -->
					<div class="inner">
						<header>
						<?php
						if (!isset($_SESSION)) { session_start(); }
						if($_SESSION["language"]=='en-US'){
							$html = <<<HTML
								<h1 id="logo">Resume</h1>
							<hr />
							<p>My skills</p>
HTML;
							echo $html;
							}
					  
						elseif($_SESSION["language"]=='fr-FR'){
							$html = <<<HTML
								<h1 id="logo">Curriculum Vitae</h1>
							<hr />
							<p>Mes atouts</p>
HTML;
							echo $html;
						}
					  
					?>
                    
							
						</header>
						<footer>
								<a href="#go" class="button circled scrolly icon fas fa-arrow-down"></a>
						</footer>
					</div>

					<!-- Nav -->
					<?php
					  	require_once('./assets/thumbnail/menu.php');
					  	generate_menu();
 					?>	
 				</div>

				<div id="go" class="wrapper style1">
					<button onclick="topFunction()" id="myBtn" title="Go to top" class="icon fas fa-arrow-up"></button>
					<section class="container special">
						
                	 	<header>
						 <?php
						 if (!isset($_SESSION)) { session_start(); }
						if($_SESSION["language"]=='en-US'){
							$html = <<<HTML
								<span id="all"  class="btnmenu">All</span>
		                   <span  id="experience" class="btnmenu">Experience</span>
		                    <span id="formation" class="btnmenu">Education</span>
		                    <span id="competence" class="btnmenu">Skills</span><br>
		                    <a href="realisation/CV_sophie-boyer" target="_blank" id="download-cv"> <span  class="btnmenu">Download my resume</span></a>
               			</header>
               			<div id="block-experience" class="row title-section">
							<div class="col-md-12">
								<h2>Experience</h2>
								<div id="main-timeline-experience" class="main-timeline">
										
								</div>
							</div>
						</div>
						<div id="block-formation" class="row title-section">
							<div class="col-md-12">
								<h2>Education</h2>
								<div id="main-timeline-formation" class="main-timeline">
										
								</div>
							</div>
						</div>
						<div id="block-skill" class="title-section">
								<h2>Skills</h2>
								<div id="main-timeline-skills" class="">
									
										
								</div>
							
						</div>
HTML;
							echo $html;
							}
					  
						elseif($_SESSION["language"]=='fr-FR'){
							$html = <<<HTML
								  <span id="all"  class="btnmenu">Tout</span>
		                   <span  id="experience" class="btnmenu">Expériences</span>
		                    <span id="formation" class="btnmenu">Formations</span>
		                    <span id="competence" class="btnmenu">Compétences</span><br>
		                    <a href="realisation/CV_sophie-boyer" target="_blank" id="download-cv"> <span  class="btnmenu">Télécharger mon CV</span></a>
               			</header>
               			<div id="block-experience" class="row title-section">
							<div class="col-md-12">
								<h2>Expériences</h2>
								<div id="main-timeline-experience" class="main-timeline">
										
								</div>
							</div>
						</div>
						<div id="block-formation" class="row title-section">
							<div class="col-md-12">
								<h2>Formations</h2>
								<div id="main-timeline-formation" class="main-timeline">
										
								</div>
							</div>
						</div>
						<div id="block-skill" class="title-section">
								<h2>Compétences</h2>
								<div id="main-timeline-skills" class="">
									
										
								</div>
							
						</div>
HTML;
							echo $html;
						}
					  
					?>
		                  
					
					   

             		</section>
				</div>
				<!-- Footer -->
				<?php
					require_once('./assets/thumbnail/footer.php');
					generate_footer();
				?>	

			</div>


		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.onvisible.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
			<script src="assets/js/button.js"></script>
			<script src="assets/js/resume.js"></script>
			

	</body>

</html>
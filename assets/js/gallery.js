// document ready in ES6
Document.prototype.ready = callback => {
    if (callback && typeof callback === 'function') {
        document.addEventListener("DOMContentLoaded", () => {
            if (document.readyState === "interactive" || document.readyState === "complete") {
                return callback();
            }
        });
    }
};



(function() {
    var name = $_GET('project');


    // queries construction
    let params = {};
   // var part = "http://localhost/sophieboyer.fr/";
    var part="https://sophieboyer.fr/";
    let url = new URL("assets/php/gallery.php", part);
    if (name == null) {
        params['project'] = "all";
        params['category'] = "all";
        document.getElementById("all").onclick = event => {
            params['category'] = "all";
            selectTheme(params['category'], part);
            document.getElementById("all").style.background = "white";
            document.getElementById("all").style.color = "rgba(54, 51, 72, 0.5)";
            document.getElementById("all").style.borderBottom = "1px solid rgba(54, 51, 72, 0.5)";

            document.getElementById("web").style.color = "white";
            document.getElementById("web").style.background = "rgba(54, 51, 72, 0.5)";
             document.getElementById("graphisme").style.color = "white";
            document.getElementById("graphisme").style.background = "rgba(54, 51, 72, 0.5)";
            document.getElementById("audiovisuel").style.color = "white";
            document.getElementById("audiovisuel").style.background = "rgba(54, 51, 72, 0.5)";
            document.getElementById("jeux vidéo").style.color = "white";
            document.getElementById("jeux vidéo").style.background = "rgba(54, 51, 72, 0.5)";
            // document.getElementById("communication").style.color = "white";
            // document.getElementById("communication").style.background = "rgba(54, 51, 72, 0.5)";

        };
        document.getElementById("web").onclick = event => {
            params['category'] = "web";
            selectTheme(params['category'], part);
            document.getElementById("web").style.background = "white";
            document.getElementById("web").style.color = "rgba(54, 51, 72, 0.5)";
            document.getElementById("web").style.borderBottom = "1px solid rgba(54, 51, 72, 0.5)";

            document.getElementById("graphisme").style.color = "white";
            document.getElementById("graphisme").style.background = "rgba(54, 51, 72, 0.5)";
            document.getElementById("audiovisuel").style.color = "white";
            document.getElementById("audiovisuel").style.background = "rgba(54, 51, 72, 0.5)";
            document.getElementById("jeux vidéo").style.color = "white";
            document.getElementById("jeux vidéo").style.background = "rgba(54, 51, 72, 0.5)";
            // document.getElementById("communication").style.color = "white";
            // document.getElementById("communication").style.background = "rgba(54, 51, 72, 0.5)";
            document.getElementById("all").style.color = "white";
            document.getElementById("all").style.background = "rgba(54, 51, 72, 0.5)";
        };
        document.getElementById("graphisme").onclick = event => {
            params['category'] = "graphisme";
            selectTheme(params['category'], part);
            document.getElementById("graphisme").style.background = "white";
            document.getElementById("graphisme").style.color = "rgba(54, 51, 72, 0.5)";
            document.getElementById("graphisme").style.borderBottom = "1px solid rgba(54, 51, 72, 0.5)";

            document.getElementById("web").style.color = "white";
            document.getElementById("web").style.background = "rgba(54, 51, 72, 0.5)";
            document.getElementById("audiovisuel").style.color = "white";
            document.getElementById("audiovisuel").style.background = "rgba(54, 51, 72, 0.5)";
            document.getElementById("jeux vidéo").style.color = "white";
            document.getElementById("jeux vidéo").style.background = "rgba(54, 51, 72, 0.5)";
            // document.getElementById("communication").style.color = "white";
            // document.getElementById("communication").style.background = "rgba(54, 51, 72, 0.5)";
             document.getElementById("all").style.color = "white";
            document.getElementById("all").style.background = "rgba(54, 51, 72, 0.5)";
        };
        document.getElementById("audiovisuel").onclick = event => {
            params['category'] = "audiovisuel";
            selectTheme(params['category'], part);
             document.getElementById("audiovisuel").style.background = "white";
            document.getElementById("audiovisuel").style.color = "rgba(54, 51, 72, 0.5)";
            document.getElementById("audiovisuel").style.borderBottom = "1px solid rgba(54, 51, 72, 0.5)";

            document.getElementById("web").style.color = "white";
            document.getElementById("web").style.background = "rgba(54, 51, 72, 0.5)";
            document.getElementById("graphisme").style.color = "white";
            document.getElementById("graphisme").style.background = "rgba(54, 51, 72, 0.5)";
            document.getElementById("jeux vidéo").style.color = "white";
            document.getElementById("jeux vidéo").style.background = "rgba(54, 51, 72, 0.5)";
            // document.getElementById("communication").style.color = "white";
            // document.getElementById("communication").style.background = "rgba(54, 51, 72, 0.5)";
             document.getElementById("all").style.color = "white";
            document.getElementById("all").style.background = "rgba(54, 51, 72, 0.5)";
        };
        document.getElementById("jeux vidéo").onclick = event => {
            params['category'] = "jeux vidéo";
            selectTheme(params['category'], part);
             document.getElementById("jeux vidéo").style.background = "white";
            document.getElementById("jeux vidéo").style.color = "rgba(54, 51, 72, 0.5)";
            document.getElementById("jeux vidéo").style.borderBottom = "1px solid rgba(54, 51, 72, 0.5)";

            document.getElementById("web").style.color = "white";
            document.getElementById("web").style.background = "rgba(54, 51, 72, 0.5)";
            document.getElementById("graphisme").style.color = "white";
            document.getElementById("graphisme").style.background = "rgba(54, 51, 72, 0.5)";
            document.getElementById("audiovisuel").style.color = "white";
            document.getElementById("audiovisuel").style.background = "rgba(54, 51, 72, 0.5)";
            // document.getElementById("communication").style.color = "white";
            // document.getElementById("communication").style.background = "rgba(54, 51, 72, 0.5)";
             document.getElementById("all").style.color = "white";
            document.getElementById("all").style.background = "rgba(54, 51, 72, 0.5)";
        };
        // document.getElementById("communication").onclick = event => {
        //     params['category'] = "communication";
        //     selectTheme(params['category'], part);
        //     document.getElementById("communication").style.background = "white";
        //     document.getElementById("communication").style.color = "rgba(54, 51, 72, 0.5)";
        //     document.getElementById("communication").style.borderBottom = "1px solid rgba(54, 51, 72, 0.5)";

        //     document.getElementById("web").style.color = "white";
        //     document.getElementById("web").style.background = "rgba(54, 51, 72, 0.5)";
        //     document.getElementById("graphisme").style.color = "white";
        //     document.getElementById("graphisme").style.background = "rgba(54, 51, 72, 0.5)";
        //     document.getElementById("audiovisuel").style.color = "white";
        //     document.getElementById("audiovisuel").style.background = "rgba(54, 51, 72, 0.5)";
        //     document.getElementById("jeux vidéo").style.color = "white";
        //     document.getElementById("jeux vidéo").style.background = "rgba(54, 51, 72, 0.5)";
        //      document.getElementById("all").style.color = "white";
        //     document.getElementById("all").style.background = "rgba(54, 51, 72, 0.5)";
        // };
        url.search = new URLSearchParams(params);
        //console.log(url);

        // GET method
        fetch(url)
            .then(response => {
                return response.json();
            })
            .then(data => {

                document.getElementById('projects').innerHTML = "";

                if (data.length == 0) {
                    document.getElementById('go').appendChild(document.createTextNode("Nothing Found"));

                } else {

                    data.forEach(function(element) {

                        var html = [
                            '<article class="4u 12u(mobile) special project">',
                            '<a href="assets/thumbnail/article.php?project=' + element.link + '" target="_blank">',
                            '<div class="grid">',
                            '<figure class="effect-sarah">',
                            '<img class="imagegallery" src="images/' + element.image + '" alt="' + element.image + '"/>',
                            '<figcaption>',
                            '<p class="title_project">' + element.name + '</p>',
                            '<p>' + element.description + '</p>',

                            '</figcaption>',
                            '</figure>',
                            '</div>',

                            '</a>',
                            '</article>',
                        ].join("\n");

                        document.getElementById('projects').insertAdjacentHTML('beforeend', html);


                    });
                    //  console.log(data);
                }
            })
            .catch(error => {
                //console.log(error);
            });
        // bloc catch appelé lorsqu'il y a une erreur
    } else {
        params['project'] = $_GET('project');

        url.search = new URLSearchParams(params);
        fetch(url)
            .then(response => {
                return response.json();
            })
            .then(data => {
                document.getElementById('logo').innerHTML = "";
                if (data.length == 0) {
                    document.getElementById('item-description').appendChild(document.createTextNode("Nothing Found"));
                } else {
                    data.forEach(function(element) {
                        document.getElementById('logo').innerHTML = element.name;
                        if(element.language=="fr-FR"){
                            var html = [
                                '<div class="row">',
    
                                '<div class="col-md-8 padding-left">',
                                '<p>' + element.description_long + '</p>',
                                '</div>  ',
                                '<div class="col-md-3 col-md-offset-1">',
                                '<div class="row">',
    
                                '<div class="col-md-4 col-xs-12 item-element-title">',
                                ' <p>Logiciels</p>',
                                '</div>',
                                '<div class="col-md-8 col-xs-12 item-element">',
                                '<p>' + element.software + '</p>',
                                '</div>',
                                 '<div class="col-md-4 col-xs-12 item-element-title  padding-none">',
                                
                                '<p>Postes</p>',
                               
                                '</div>',
                                '<div class="col-md-8 col-xs-12 item-element padding-none">',
                                
                                '<p>' + element.post + '</p>',
                            
                                '</div>',
                                 '<div class="col-md-4 col-xs-12 item-element-title padding-none">',
                           
                                '<p>Date</p>',
                                '</div>',
                                '<div class="col-md-8 col-xs-12 item-element padding-none">',
                               
                                '<p>' + element.date + '</p>',
                                '</div>',
    
                                '</div>  ',
                                '<div class="row">',
    
                                '<div id="access-project" class="col-md-12 item-button">',
    
                                '</div>',
    
    
                                '</div>  ',
                                '</div>',
    
    
                                '</div>',
                            ].join("\n");
                        }
                        else{
                            var html = [
                                '<div class="row">',
    
                                '<div class="col-md-8 padding-left">',
                                '<p>' + element.description_long + '</p>',
                                '</div>  ',
                                '<div class="col-md-3 col-md-offset-1">',
                                '<div class="row">',
    
                                '<div class="col-md-4 col-xs-12 item-element-title">',
                                ' <p>Softwares</p>',
                                '</div>',
                                '<div class="col-md-8 col-xs-12 item-element">',
                                '<p>' + element.software + '</p>',
                                '</div>',
                                 '<div class="col-md-4 col-xs-12 item-element-title  padding-none">',
                                
                                '<p>Post</p>',
                               
                                '</div>',
                                '<div class="col-md-8 col-xs-12 item-element padding-none">',
                                
                                '<p>' + element.post + '</p>',
                            
                                '</div>',
                                 '<div class="col-md-4 col-xs-12 item-element-title padding-none">',
                           
                                '<p>Date</p>',
                                '</div>',
                                '<div class="col-md-8 col-xs-12 item-element padding-none">',
                               
                                '<p>' + element.date + '</p>',
                                '</div>',
    
                                '</div>  ',
                                '<div class="row">',
    
                                '<div id="access-project" class="col-md-12 item-button">',
    
                                '</div>',
    
    
                                '</div>  ',
                                '</div>',
    
    
                                '</div>',
                            ].join("\n");
                        }
                       
                       


                        document.getElementById('item-description').insertAdjacentHTML('beforeend', html);
                        
                        if (element.access != "") {
                            if(element.language=="fr-FR"){
                            var html5 = [
                                '<a href="' + element.access + '" target="_blank" class="button">Accéder au projet</a>',
                            ].join("\n");
                            }
                            else{
                                var html5 = [
                                    '<a href="' + element.access + '" target="_blank" class="button">Access to the project</a>',
                                ].join("\n");
                            }
                            document.getElementById('access-project').insertAdjacentHTML('beforeend', html5);
                        }
                        var images = element.images;
                        var tmp = 0;
                        console.log(images.length);
                        images.forEach(function(element) {

                            if (tmp == 0) {
                                var html2 = [

                                    '<div class="item active">',
                                    '<img src="../../images/' + element + '" alt="" style="width:100%;">',
                                    '</div>',
                                ].join("\n");
                                if (images.length > 1) {
                                    var html3 = [

                                        '<li data-target="#myCarousel" data-slide-to="' + tmp + '" class="active"></li>',
                                    ].join("\n");
                                    document.getElementById('carousel-indicators').insertAdjacentHTML('beforeend', html3);
                                }

                                tmp++;
                            } else {
                                var html2 = [

                                    '<div class="item ">',
                                    '<img src="../../images/' + element + '" alt="" style="width:100%;">',
                                    '</div>',
                                ].join("\n");
                                var html3 = [

                                    '<li data-target="#myCarousel" data-slide-to="' + tmp + '"></li>',
                                ].join("\n");
                                document.getElementById('carousel-indicators').insertAdjacentHTML('beforeend', html3);
                                tmp++;
                            }
                            if (tmp > 1) {
                                var html4 = [
                                    '<a class="left carousel-control" href="#myCarousel" data-slide="prev">',
                                    '<span class="glyphicon glyphicon-chevron-left"></span>',
                                    '<span class="sr-only">Précédent</span>',
                                    '</a>',
                                    '<a class="right carousel-control" href="#myCarousel" data-slide="next">',
                                    '<span class="glyphicon glyphicon-chevron-right"></span>',
                                    '<span class="sr-only">Suivant</span>',
                                ].join("\n");
                                document.getElementById('myCarousel').insertAdjacentHTML('beforeend', html4);
                            }


                            document.getElementById('carousel-inner').insertAdjacentHTML('beforeend', html2);


                        });


                    });
                    //console.log(data);
                }
            })
            .catch(error => {
                //console.log(error);
            });
        // bloc catch appelé lorsqu'il y a une erreur
    }



})();

function selectTheme(category, part) {
    // queries construction
    let params = {};
    params['project'] = "all";
    params['category'] = category;

    //let url = new URL("assets/php/gallery.php", part);
    let url = new URL("assets/php/gallery.php", part);
    url.search = new URLSearchParams(params);
    //console.log(url);

    // GET method
    fetch(url)
        .then(response => {
            return response.json();
        })
        .then(data => {
            document.getElementById('projects').innerHTML = "";
            if (data.length == 0) {
                document.getElementById('go').appendChild(document.createTextNode("Nothing Found"));
            } else {
                data.forEach(function(element) {

                    var html = [

                        '<article class="4u 12u(mobile) special project">',
                       '<a href="assets/thumbnail/article.php?project=' + element.link + '" target="_blank">',
                        '<div class="grid">',
                        '<figure class="effect-sarah">',
                        '<img class="imagegallery" src="images/' + element.image + '" alt="' + element.image + '"/>',
                        '<figcaption>',
                        '<p class="title_project">' + element.name + '</p>',
                        '<p>' + element.description + '</p>',

                        '</figcaption>',
                        '</figure>',
                        '</div>',

                        '</a>',
                        '</article>',
                    ].join("\n");

                    document.getElementById('projects').insertAdjacentHTML('beforeend', html);

                });
                //console.log(data);
            }
        })
        .catch(error => {
            //console.log(error);
        });
    // bloc catch appelé lorsqu'il y a une erreur
}

// function onClick(id, part){
//     let params = {};
//     params['project'] = id;
//     //let url = new URL("assets/php/gallery.php", part);
//     let url = new URL("assets/php/gallery.php", part);
//     url.search = new URLSearchParams(params);
//     //console.log(url);

//     // GET method
//     fetch(url)
//         .then(response => {
//             return response.json();
//         })
//         .then(data => {
//             if (data.length == 0) {
//                 document.getElementById('go').appendChild(document.createTextNode("Nothing Found"));
//             } else {
//                 data.forEach(function(element) {

//                     var html = [

//                         '<article class="4u 12u(mobile) special">',
//                         '<a href="articles/'+element.link+'" target="_blank">',
//                            '<div class="grid">',
//                                 '<figure class="effect-sarah">',
//                                     '<img class="imagegallery" src="images/'+element.image+'" alt="'+element.image+'"/>',
//                                     '<figcaption>',

//                                         '<p>'+element.description+'</p>',

//                                     '</figcaption>',         
//                                 '</figure>',
//                             '</div>',
//                             '<h3 class="title">'+element.name+'</h3>',
//                             '</a>',
//                         '</article>',
//                     ].join("\n");

//                     if(element.category=="web"){
//                         document.getElementById('webrow').insertAdjacentHTML('beforeend', html);
//                     }
//                     else if(element.category=="graphisme"){
//                         document.getElementById('graphismerow').insertAdjacentHTML('beforeend', html);
//                     }
//                     else if(element.category=="audiovisuel"){
//                         document.getElementById('montagerow').insertAdjacentHTML('beforeend', html);
//                     }
//                     else if(element.category=="jeux vidéo"){
//                         document.getElementById('jeurow').insertAdjacentHTML('beforeend', html);
//                     }
//                     else if(element.category=="communication"){
//                         document.getElementById('communicationrow').insertAdjacentHTML('beforeend', html);
//                     }
//                     onClick(element.id_project, part);
//                 });
//                 //console.log(data);
//             }
//         })
//         .catch(error => {
//             //console.log(error);
//         });
// }

function $_GET(param) {
    var vars = {};
    window.location.href.replace(location.hash, '').replace(
        /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
        function(m, key, value) { // callback
            vars[key] = value !== undefined ? value : '';
        }
    );

    if (param) {
        return vars[param] ? vars[param] : null;
    }
    return vars;
}
// document ready in ES6
Document.prototype.ready = callback => {
    if (callback && typeof callback === 'function') {
        document.addEventListener("DOMContentLoaded", () => {
            if (document.readyState === "interactive" || document.readyState === "complete") {
                return callback();
            }
        });
    }
};



(function() {

    // queries construction
    let params = {};
  //  var part = "http://localhost/sophieboyer.fr/";
    var part="https://sophieboyer.fr/";
    let url = new URL("assets/php/languages.php", part);
    url.search = new URLSearchParams(params);

    // GET method
    fetch(url)
        .then(response => {
            return response.json();
        })
        .then(data => {
            if (data.length == 0) {
                document.getElementById('go').appendChild(document.createTextNode("Nothing Found"));
            } else {
                  console.log(data);
                  click();
            }
        })
        .catch(error => {
            //console.log(error);
        });


  

})();


function click(){
    if(document.getElementById("fr-FR")){
     
        document.getElementById("fr-FR").onclick = event => {
             let params = {};
            params['language'] = "fr-FR";
        //    part = "http://localhost/sophieboyer.fr/";
          part="https://sophieboyer.fr/";
            url = new URL("assets/php/languages.php", part);
            url.search = new URLSearchParams(params);

            // GET method
            fetch(url)
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    if (data.length == 0) {
                        document.getElementById('go').appendChild(document.createTextNode("Nothing Found"));
                    } else {
                        console.log(data);
                    }
                })
                .catch(error => {
                    //console.log(error);
                });
            // bloc catch appelé lorsqu'il y a une erreur
            location.reload();
        }
    }

    if( document.getElementById("en-US")){
        document.getElementById("en-US").onclick = event => {
            let params = {};
            params['language'] = "en-US";
        //    part = "http://localhost/sophieboyer.fr/";
          part="https://sophieboyer.fr/";
            url = new URL("assets/php/languages.php", part);
            url.search = new URLSearchParams(params);
    
            // GET method
            fetch(url)
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    if (data.length == 0) {
                        document.getElementById('go').appendChild(document.createTextNode("Nothing Found"));
                    } else {
                        console.log(data);
                    }
                })
                .catch(error => {
                    //console.log(error);
                });
            // bloc catch appelé lorsqu'il y a une erreur
            location.reload();
        }
    
    }
    
}
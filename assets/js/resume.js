// document ready in ES6
Document.prototype.ready = callback => {
    if (callback && typeof callback === 'function') {
        document.addEventListener("DOMContentLoaded", () => {
            if (document.readyState === "interactive" || document.readyState === "complete") {
                return callback();
            }
        });
    }
};

(function() {


    document.getElementById("all").onclick = event => {
        document.getElementById("block-formation").style.display = "block";
        document.getElementById("block-experience").style.display = "block";
        document.getElementById("block-skill").style.display = "block";
        document.getElementById("all").style.background = "white";
        document.getElementById("all").style.color = "rgba(54, 51, 72, 0.5)";
        document.getElementById("all").style.borderBottom = "1px solid rgba(54, 51, 72, 0.5)";

        document.getElementById("experience").style.color = "white";
        document.getElementById("experience").style.background = "rgba(54, 51, 72, 0.5)";
        document.getElementById("formation").style.color = "white";
        document.getElementById("formation").style.background = "rgba(54, 51, 72, 0.5)";
        document.getElementById("competence").style.color = "white";
        document.getElementById("competence").style.background = "rgba(54, 51, 72, 0.5)";
    };
    document.getElementById("experience").onclick = event => {
        document.getElementById("block-formation").style.display = "none";
        document.getElementById("block-experience").style.display = "block";
        document.getElementById("block-skill").style.display = "none";
        document.getElementById("experience").style.background = "white";
        document.getElementById("experience").style.color = "rgba(54, 51, 72, 0.5)";
        document.getElementById("experience").style.background = "1px solid rgba(54, 51, 72, 0.5)";

        document.getElementById("all").style.color = "white";
        document.getElementById("all").style.background = "rgba(54, 51, 72, 0.5)";
        document.getElementById("formation").style.color = "white";
        document.getElementById("formation").style.background = "rgba(54, 51, 72, 0.5)";
        document.getElementById("competence").style.color = "white";
        document.getElementById("competence").style.background = "rgba(54, 51, 72, 0.5)";
    };    
    document.getElementById("formation").onclick = event => {
        document.getElementById("block-formation").style.display = "block";
        document.getElementById("block-experience").style.display = "none";
        document.getElementById("block-skill").style.display = "none";
        document.getElementById("formation").style.background = "white";
        document.getElementById("formation").style.color = "rgba(54, 51, 72, 0.5)";
        document.getElementById("formation").style.borderBottom = "1px solid rgba(54, 51, 72, 0.5)";

        document.getElementById("experience").style.color = "white";
        document.getElementById("experience").style.background = "rgba(54, 51, 72, 0.5)";
        document.getElementById("all").style.color = "white";
        document.getElementById("all").style.background = "rgba(54, 51, 72, 0.5)";
        document.getElementById("competence").style.color = "white";
        document.getElementById("competence").style.background = "rgba(54, 51, 72, 0.5)";
    };
    document.getElementById("competence").onclick = event => {
        document.getElementById("block-formation").style.display = "none";
        document.getElementById("block-experience").style.display = "none";
        document.getElementById("block-skill").style.display = "block";
        document.getElementById("competence").style.background = "white";
        document.getElementById("competence").style.color = "rgba(54, 51, 72, 0.5)";
        document.getElementById("competence").style.background = "1px solid rgba(54, 51, 72, 0.5)";

        document.getElementById("experience").style.color = "white";
        document.getElementById("experience").style.background = "rgba(54, 51, 72, 0.5)";
        document.getElementById("formation").style.color = "white";
        document.getElementById("formation").style.background = "rgba(54, 51, 72, 0.5)";
        document.getElementById("all").style.color = "white";
        document.getElementById("all").style.background = "rgba(54, 51, 72, 0.5)";
    };

    // queries construction
    let params = {};
   // var part = "http://localhost/sophieboyer.fr/";
    var part="https://sophieboyer.fr/";
    params['job'] = "all";
    params['category'] = "all";
    let url = new URL("assets/php/resume.php", part);
    url.search = new URLSearchParams(params);
    //console.log(url);

    // GET method
    fetch(url)
        .then(response => {
            return response.json();
        })
        .then(data => {
            if (data.length == 0) {
                document.getElementById('main-timeline-experience').appendChild(document.createTextNode("Nothing Found"));
            } else {
                data.forEach(function(element) {
                    var html = [
                        '<div class="timeline">',
                        '<div href="" class="timeline-content">',
                        '<div class="timeline-icon">',
                        '<img class="logo-company" src="images/' + element.image + '" alt=""/>',
                        '</div>',
                        '<h3 class="title-timeline">' + element.name + '</h3>',
                        '<p class="description-timeline">' + element.description + '</p>',
                        '<p class="description-timeline timeline-company">' + element.location + ' - ' + element.date + ' </p>',
                        '</div>',
                        '</div>',
                    ].join("\n");
                    document.getElementById('main-timeline-experience').insertAdjacentHTML('beforeend', html);

                });
                //console.log(data);
            }
        })
        .catch(error => {
            //console.log(error);
        });
    // bloc catch appelé lorsqu'il y a une erreur

    params['education'] = "all";
    url = new URL("assets/php/education.php", part);
    url.search = new URLSearchParams(params);

    // GET method
    fetch(url)
        .then(response => {
            return response.json();
        })
        .then(data => {
            if (data.length == 0) {
                document.getElementById('main-timeline-formation').appendChild(document.createTextNode("Nothing Found"));
            } else {
                data.forEach(function(element) {
                    var html = [
                        '<div class="timeline">',
                        '<div href="" class="timeline-content">',
                        '<div class="timeline-icon">',
                        '<img class="logo-company" src="images/' + element.image + '" alt=""/>',
                        '</div>',
                        '<h3 class="title-timeline">' + element.name + '</h3>',
                        '<p class="description-timeline">' + element.description + '</p>',
                        '<p class="description-timeline timeline-company">' + element.location + ' - Promo ' + element.date + ' </p>',
                        '</div>',
                        '</div>',
                    ].join("\n");
                    document.getElementById('main-timeline-formation').insertAdjacentHTML('beforeend', html);

                });
                //console.log(data);
            }
        })
        .catch(error => {
            //console.log(error);
        });
    // bloc catch appelé lorsqu'il y a une erreur

    params['skill'] = "all";
    params['category'] = "all";
    url = new URL("assets/php/skill.php", part);
    url.search = new URLSearchParams(params);

    // GET method
    fetch(url)
        .then(response => {
            return response.json();
        })
        .then(data => {
            if (data.length == 0) {
                document.getElementById('main-timeline-skills').appendChild(document.createTextNode("Nothing Found"));
            } else {
                data.forEach(function(element) {
                    var html = [
                        '<h3 id="title-' + element.category + '" class="title-skills">' + element.category + '</h3>',
                        '<div id="block-' + element.category + '" class="row block content-skills"></div>',
                    ].join("\n");
                    document.getElementById('main-timeline-skills').insertAdjacentHTML('beforeend', html);

                    params['skill'] = "all";
                    params['category'] = element.category;
                    url = new URL("assets/php/skill.php", part);
                    url.search = new URLSearchParams(params);

                    // GET method
                    fetch(url)
                        .then(response => {
                            return response.json();
                        })
                        .then(data => {
                            if (data.length == 0) {
                                document.getElementById('block-' + element.category).appendChild(document.createTextNode("Nothing Found"));
                            } else {
                                data.forEach(function(element) {
                                    var html = [
                                        '<div class="col-sm-3 col-md-2">',
                                        '<div class="progress" data-percentage="' + element.pourcentage + '">',
                                        '<span class="progress-left">',
                                        '<span class="progress-bar"></span>',
                                        '</span>',
                                        '<span class="progress-right">',
                                        '<span class="progress-bar"></span>',
                                        '</span>',
                                        '<div class="progress-value">',
                                        '<div>' + element.name + '<br>',
                                        '</div>',
                                        '</div>',
                                        '</div>',
                                        '</div>',
                                    ].join("\n");
                                    document.getElementById('block-' + element.category).insertAdjacentHTML('beforeend', html);

                                });
                                //console.log(data);
                            }
                        })
                        .catch(error => {
                            //console.log(error);
                        });
                    // bloc catch appelé lorsqu'il y a une erreur



                });
                //console.log(data);
            }
        })
        .catch(error => {
            //console.log(error);
        });
    // bloc catch appelé lorsqu'il y a une erreur



})();
<?php
session_start();

header("Content-Type: application/json; charset=UTF-8");

include_once "./connect.php";

$method = strtolower($_SERVER['REQUEST_METHOD']);

if ($method !== 'get') {
    http_response_code(405);
    echo json_encode(array(
        'message' => 'This method is not allowed.'
    ));
    exit();
}

//check what we have to show
if (isset($_GET['skill']) && isset($_GET['category'])  ) {
    $item['skill']     = $_GET['skill'];
    $item['category']     = $_GET['category'];
}
else {
    http_response_code(404);
    echo json_encode("No request provided");
    exit();
}

//SQL COMMAND
$skills = array();
if($_SESSION["language"]=="fr-FR"){
    if($item['skill']=="all"){
        if($item['category']=="all"){
            $stmt = MyPDO::getInstance()->prepare(<<<SQL
                SELECT DISTINCT Skills.category
                FROM Skills;
SQL
            ); 
    
            if($stmt->execute()){
                while (($row = $stmt->fetch()) !== false) {
                    array_push($skills, $row);
                }
                echo json_encode($skills);
                http_response_code(200);
                exit();
            }
            else{
                $message = array(
                    "Message" => "Error",
                    "code" => 1
                );
                echo json_encode($message);
                exit();
            }
    
        }
        else{
            $stmt = MyPDO::getInstance()->prepare(<<<SQL
                SELECT  *
                FROM Skills
                WHERE Skills.category=:category;
SQL
            ); 
            if($stmt->execute(['category' => $item['category']])){
                while (($row = $stmt->fetch()) !== false) {
                    array_push($skills, $row);
                }
                echo json_encode($skills);
                http_response_code(200);
                exit();
            }
            else{
                $message = array(
                    "Message" => "Error",
                    "code" => 1
                );
                echo json_encode($message);
                exit();
            }
        }
    }
    else{
        session_start();
        $_SESSION["skillOpen"] = $item['skill'];
        exit();
    
    }
}
else{
    if($item['skill']=="all"){
        if($item['category']=="all"){
            $stmt = MyPDO::getInstance()->prepare(<<<SQL
                SELECT DISTINCT Skills_en.category
                FROM Skills_en;
SQL
            ); 
    
            if($stmt->execute()){
                while (($row = $stmt->fetch()) !== false) {
                    array_push($skills, $row);
                }
                echo json_encode($skills);
                http_response_code(200);
                exit();
            }
            else{
                $message = array(
                    "Message" => "Error",
                    "code" => 1
                );
                echo json_encode($message);
                exit();
            }
    
        }
        else{
            $stmt = MyPDO::getInstance()->prepare(<<<SQL
                SELECT  *
                FROM Skills_en
                WHERE Skills_en.category=:category;
SQL
            ); 
            if($stmt->execute(['category' => $item['category']])){
                while (($row = $stmt->fetch()) !== false) {
                    array_push($skills, $row);
                }
                echo json_encode($skills);
                http_response_code(200);
                exit();
            }
            else{
                $message = array(
                    "Message" => "Error",
                    "code" => 1
                );
                echo json_encode($message);
                exit();
            }
        }
    }
    else{
        session_start();
        $_SESSION["skillOpen"] = $item['skill'];
        exit();
    
    }
}




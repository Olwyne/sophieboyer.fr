<?php
session_start();

header("Content-Type: application/json; charset=UTF-8");

include_once "./connect.php";

$method = strtolower($_SERVER['REQUEST_METHOD']);

if ($method !== 'get') {
    http_response_code(405);
    echo json_encode(array(
        'message' => 'This method is not allowed.'
    ));
    exit();
}

//check what we have to show
if (isset($_GET['project'])) {
    $item['project']     = $_GET['project'];
     if(isset($_GET['category']) ){
        $item['category']     = $_GET['category'];
    }

}
else {
    http_response_code(404);
    echo json_encode("No request provided");
    exit();
}

//SQL COMMAND
$projects = array();
$images = array();
if($_SESSION["language"]=="fr-FR"){
    if($item['project']=="all"){

        if($item['category']=="all"){
            $stmt = MyPDO::getInstance()->prepare(<<<SQL
                SELECT  *
                FROM Projects
                WHERE Projects.show=1
                ORDER BY date DESC, id_project DESC;
SQL
            ); 
            if($stmt->execute()){
                while (($row = $stmt->fetch()) !== false) {
                    array_push($projects, $row);
                }
                echo json_encode($projects);
                http_response_code(200);
                exit();
            }
            else{
                $message = array(
                    "Message" => "Error",
                    "code" => 1
                );
                echo json_encode($message);
                exit();
            }
        }
        else{
            $stmt = MyPDO::getInstance()->prepare(<<<SQL
                SELECT  *
                FROM Projects
                WHERE Projects.show=1
                AND Projects.category=:category
                ORDER BY date DESC, id_project DESC;
SQL
            ); 
            if($stmt->execute(['category' => $item['category']])){
                while (($row = $stmt->fetch()) !== false) {
                    array_push($projects, $row);
                }
                echo json_encode($projects);
                http_response_code(200);
                exit();
            }
            else{
                $message = array(
                    "Message" => "Error",
                    "code" => 1
                );
                echo json_encode($message);
                exit();
            }
        }
        
      
    }
    else{
    
       $stmt = MyPDO::getInstance()->prepare(<<<SQL
                SELECT  *
                FROM Projects
                WHERE Projects.link=:name;
SQL
            ); 
            if($stmt->execute(['name' => $item['project']])){
                while (($row = $stmt->fetch()) !== false) {
                    array_push($projects, $row);
                }
                foreach ($projects as $key => $project) {
                    $stmt = MyPDO::getInstance()->prepare(<<<SQL
                SELECT  *
                FROM Projects, Images
                WHERE Projects.link=:name
                AND Projects.id_project=Images.id_project;
SQL
            ); 
                 if($stmt->execute(['name' => $item['project']])){
                   while (($row = $stmt->fetch()) !== false) {
                        array_push($images, $row['path']);
                         }
                        $projects[$key]['images'] = $images;
                        $projects[$key]['language'] = $_SESSION["language"];
                        echo json_encode($projects);
                http_response_code(200);
                exit();
                }
                }
                
    
    
                
            }
            else{
                $message = array(
                    "Message" => "Error",
                    "code" => 1
                );
                echo json_encode($message);
                exit();
            }
        exit();
    
    }
}
else{
    if($item['project']=="all"){

        if($item['category']=="all"){
            $stmt = MyPDO::getInstance()->prepare(<<<SQL
                SELECT  *
                FROM Projects_en
                WHERE Projects_en.show=1
                ORDER BY date DESC, id_project DESC;
SQL
            ); 
            if($stmt->execute()){
                while (($row = $stmt->fetch()) !== false) {
                    array_push($projects, $row);
                }
                echo json_encode($projects);
                http_response_code(200);
                exit();
            }
            else{
                $message = array(
                    "Message" => "Error",
                    "code" => 1
                );
                echo json_encode($message);
                exit();
            }
        }
        else{
            $stmt = MyPDO::getInstance()->prepare(<<<SQL
                SELECT  *
                FROM Projects_en
                WHERE Projects_en.show=1
                AND Projects_en.category=:category
                ORDER BY date DESC, id_project DESC;
SQL
            ); 
            if($stmt->execute(['category' => $item['category']])){
                while (($row = $stmt->fetch()) !== false) {
                    array_push($projects, $row);
                }
                echo json_encode($projects);
                http_response_code(200);
                exit();
            }
            else{
                $message = array(
                    "Message" => "Error",
                    "code" => 1
                );
                echo json_encode($message);
                exit();
            }
        }
        
      
    }
    else{
    
       $stmt = MyPDO::getInstance()->prepare(<<<SQL
                SELECT  *
                FROM Projects_en
                WHERE Projects_en.link=:name;
SQL
            ); 
            if($stmt->execute(['name' => $item['project']])){
                while (($row = $stmt->fetch()) !== false) {
                    array_push($projects, $row);
                }
                foreach ($projects as $key => $project) {
                    $stmt = MyPDO::getInstance()->prepare(<<<SQL
                SELECT  *
                FROM Projects_en, Images
                WHERE Projects_en.link=:name
                AND Projects_en.id_project=Images.id_project;
SQL
            ); 
                 if($stmt->execute(['name' => $item['project']])){
                   while (($row = $stmt->fetch()) !== false) {
                        array_push($images, $row['path']);
                         }
                        $projects[$key]['images'] = $images;
                        $projects[$key]['language'] = $_SESSION["language"];
                        echo json_encode($projects);
                http_response_code(200);
                exit();
                }
                }
                
    
    
                
            }
            else{
                $message = array(
                    "Message" => "Error",
                    "code" => 1
                );
                echo json_encode($message);
                exit();
            }
        exit();
    
    }
}




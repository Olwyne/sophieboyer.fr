<?php
if (!isset($_SESSION)) { session_start(); }

header("Content-Type: application/json; charset=UTF-8");

include_once "./connect.php";

$method = strtolower($_SERVER['REQUEST_METHOD']);

if ($method !== 'get') {
    http_response_code(405);
    echo json_encode(array(
        'message' => 'This method is not allowed.'
    ));
    exit();
}

if (!isset($_SESSION["language"])) {
    $_SESSION["language"]="en-US";
}
elseif(isset($_GET['language'])) {
    $_SESSION["language"]=$_GET['language'];
}


$message = array(
    "Message" => "OK",
    "code" => 1,
    "language" => $_SESSION["language"]
);
echo json_encode($message);
exit();



<?php
session_start();

header("Content-Type: application/json; charset=UTF-8");

include_once "./connect.php";

$method = strtolower($_SERVER['REQUEST_METHOD']);

if ($method !== 'get') {
    http_response_code(405);
    echo json_encode(array(
        'message' => 'This method is not allowed.'
    ));
    exit();
}

//check what we have to show
if (isset($_GET['project'])) {
    $item['project']     = $_GET['project'];
}
else {
    http_response_code(404);
    echo json_encode("No request provided");
    exit();
}

//SQL COMMAND
$projects = array();
if($_SESSION["language"]=="fr-FR"){
    $stmt = MyPDO::getInstance()->prepare(<<<SQL
        SELECT  *
    	FROM Projects
        WHERE Projects.show=1
        ORDER BY RAND();
SQL
    );
    
if($stmt->execute()){
    while (($row = $stmt->fetch()) !== false) {
        array_push($projects, $row);
    }
    echo json_encode($projects);
    http_response_code(200);
    exit();
}
else{
    $message = array(
        "Message" => "Error",
        "code" => 1
    );
    echo json_encode($message);
    exit();
}
}
else{
    $stmt = MyPDO::getInstance()->prepare(<<<SQL
        SELECT  *
    	FROM Projects_en
        WHERE Projects_en.show=1
        ORDER BY RAND();
SQL
    );
    
if($stmt->execute()){
    while (($row = $stmt->fetch()) !== false) {
        array_push($projects, $row);
    }
    echo json_encode($projects);
    http_response_code(200);
    exit();
}
else{
    $message = array(
        "Message" => "Error",
        "code" => 1
    );
    echo json_encode($message);
    exit();
}
}


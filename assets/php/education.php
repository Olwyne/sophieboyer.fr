<?php
session_start();

header("Content-Type: application/json; charset=UTF-8");

include_once "./connect.php";

$method = strtolower($_SERVER['REQUEST_METHOD']);

if ($method !== 'get') {
    http_response_code(405);
    echo json_encode(array(
        'message' => 'This method is not allowed.'
    ));
    exit();
}

//check what we have to show
if (isset($_GET['education'])) {
    $item['education']     = $_GET['education'];
}
else {
    http_response_code(404);
    echo json_encode("No request provided");
    exit();
}

//SQL COMMAND
$educations = array();
if($_SESSION["language"]=="fr-FR"){
    if($item['education']=="all"){

        $stmt = MyPDO::getInstance()->prepare(<<<SQL
            SELECT  *
            FROM Educations;
SQL
        ); 
        if($stmt->execute()){
            while (($row = $stmt->fetch()) !== false) {
                array_push($educations, $row);
            }
            echo json_encode($educations);
            http_response_code(200);
            exit();
        }
        else{
            $message = array(
                "Message" => "Error",
                "code" => 1
            );
            echo json_encode($message);
            exit();
        }
}
else{
    session_start();
    $_SESSION["educationOpen"] = $item['education'];
    exit();

}
}
else{
    if($item['education']=="all"){

        $stmt = MyPDO::getInstance()->prepare(<<<SQL
            SELECT  *
            FROM Educations_en;
SQL
        ); 
        if($stmt->execute()){
            while (($row = $stmt->fetch()) !== false) {
                array_push($educations, $row);
            }
            echo json_encode($educations);
            http_response_code(200);
            exit();
        }
        else{
            $message = array(
                "Message" => "Error",
                "code" => 1
            );
            echo json_encode($message);
            exit();
        }
}
else{
    session_start();
    $_SESSION["educationOpen"] = $item['education'];
    exit();

}
}




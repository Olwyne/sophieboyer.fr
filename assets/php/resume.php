<?php
session_start();

header("Content-Type: application/json; charset=UTF-8");

include_once "./connect.php";

$method = strtolower($_SERVER['REQUEST_METHOD']);

if ($method !== 'get') {
    http_response_code(405);
    echo json_encode(array(
        'message' => 'This method is not allowed.'
    ));
    exit();
}


//check what we have to show
if (isset($_GET['job']) && isset($_GET['category'])) {
    $item['job']     = $_GET['job'];
     $item['category']     = $_GET['category'];
}
else {
    http_response_code(404);
    echo json_encode("No request provided");
    exit();
}

//SQL COMMAND
$jobs = array();
if($_SESSION["language"]=="fr-FR"){
    if($item['job']=="all"){

        if($item['category']=="all"){
    
            $stmt = MyPDO::getInstance()->prepare(<<<SQL
                SELECT  *
                FROM Jobs
                WHERE Jobs.show=1
                ORDER BY id_job DESC;
SQL
            ); 
            
            if($stmt->execute()){
                while (($row = $stmt->fetch()) !== false) {
                    array_push($jobs, $row);
                }
    
                echo json_encode($jobs);
                http_response_code(200);
                exit();
            }
            else{
                $message = array(
                    "Message" => "Error",
                    "code" => 1
                );
                echo json_encode($message);
                exit();
            }
        }
        else{
            $stmt = MyPDO::getInstance()->prepare(<<<SQL
                SELECT  *
                FROM Jobs
                WHERE Jobs.show=1
                AND Jobs.category=:category
                ORDER BY id_job DESC;
SQL
            ); 
            if($stmt->execute(['category' => $item['category']])){
                while (($row = $stmt->fetch()) !== false) {
                    array_push($jobs, $row);
                }
                echo json_encode($jobs);
                http_response_code(200);
                exit();
            }
            else{
                $message = array(
                    "Message" => "Error",
                    "code" => 1
                );
                echo json_encode($message);
                exit();
            }
        }
        
        
        
    }
    else{
        session_start();
        $_SESSION["jobOpen"] = $item['job'];
        exit();
    
    }
}
else{
    if($item['job']=="all"){

        if($item['category']=="all"){
    
            $stmt = MyPDO::getInstance()->prepare(<<<SQL
                SELECT  *
                FROM Jobs_en
                WHERE Jobs_en.show=1
                ORDER BY id_job DESC;
SQL
            ); 
            
            if($stmt->execute()){
                while (($row = $stmt->fetch()) !== false) {
                    array_push($jobs, $row);
                }
    
                echo json_encode($jobs);
                http_response_code(200);
                exit();
            }
            else{
                $message = array(
                    "Message" => "Error",
                    "code" => 1
                );
                echo json_encode($message);
                exit();
            }
        }
        else{
            $stmt = MyPDO::getInstance()->prepare(<<<SQL
                SELECT  *
                FROM Jobs_en
                WHERE Jobs_en.show=1
                AND Jobs_en.category=:category
                ORDER BY id_job DESC;
SQL
            ); 
            if($stmt->execute(['category' => $item['category']])){
                while (($row = $stmt->fetch()) !== false) {
                    array_push($jobs, $row);
                }
                echo json_encode($jobs);
                http_response_code(200);
                exit();
            }
            else{
                $message = array(
                    "Message" => "Error",
                    "code" => 1
                );
                echo json_encode($message);
                exit();
            }
        }
        
        
        
    }
    else{
        session_start();
        $_SESSION["jobOpen"] = $item['job'];
        exit();
    
    }
}




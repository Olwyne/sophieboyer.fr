<!DOCTYPE HTML>
<html>
	<head>
		<?php
		  	require_once('header_article.php');
		  	generate_head();
 		?>	
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">
			<!-- Header -->
			<div id="header">
				<!-- Inner -->
				<div class="inner">
					<header>
						<h1 id="logo">Portfolio</h1>
					</header>
				</div>
				<!-- Nav -->
				<?php
		  			require_once('menu_article.php');
		  			generate_menu();
 				?>	
			</div>
			<!-- Main -->
			<div class="wrapper style1 wrapper-carrousel">
				<div class="container">
					<article id="main" class="special">
						
					
					   
					  <div id="myCarousel" class="carousel slide" data-ride="carousel">
					    <!-- Indicators -->
					    <ol id="carousel-indicators" class="carousel-indicators">
					     
					    </ol>

					    <!-- Wrapper for slides -->
					    <div class="carousel-inner" id="carousel-inner">
					     
					    </div>

					   
					    </a>
					  </div>
				
 

<div class="container item-description" id="item-description">

</div>


					</article>
				</div>
			</div>
			<!-- Footer -->
			<?php
			  	require_once('footer.php');
			  	generate_footer();
 			?>	
		</div>
		<!-- Scripts -->
		<script src="../js/jquery.min.js"></script>
		<script src="../js/jquery.dropotron.min.js"></script>
		<script src="../js/jquery.scrolly.min.js"></script>
		<script src="../js/jquery.onvisible.min.js"></script>
		<script src="../js/skel.min.js"></script>
		<script src="../js/util.js"></script>
		<script src="../js/gallery.js"></script>
		
		<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
	
	</body>
</html>
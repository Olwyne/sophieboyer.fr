 <?php
 if (!isset($_SESSION)) { session_start(); }
 function generate_footer(){
if($_SESSION["language"]=='en-US'){
 	$html = <<<HTML
		<div id="footer">
			<div class="container">
				<div class="row">
					<div class="12u">
						<!-- Contact -->
						<section class="contact">
							<header>
								<h3>Find me</h3>
							</header>
							<ul class="icons">
								<li>
									<a href="https://www.linkedin.com/in/sophie-boyer/" class="icon fa-linkedin"><span class="label">Linkedin</span></a>
								</li>
								<!-- <li>
									<a href="https://twitter.com/sophiebyr" class="icon fa-twitter"><span class="label">Twitter</span></a>
								</li> -->
							</ul>
						</section>
						<!-- Copyright -->
						<div class="copyright">
							<ul class="menu">
								<li>&copy; SophieBoyer. All rights reserved.</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
HTML;
  echo $html;

 }

 elseif($_SESSION["language"]=='fr-FR'){
	$html = <<<HTML
	<div id="footer">
		<div class="container">
			<div class="row">
				<div class="12u">
					<!-- Contact -->
					<section class="contact">
						<header>
							<h3>Me retrouver</h3>
						</header>
						<ul class="icons">
							<li>
								<a href="https://www.linkedin.com/in/sophie-boyer/" class="icon fa-linkedin"><span class="label">Linkedin</span></a>
							</li>
							<!-- <li>
								<a href="https://twitter.com/sophiebyr" class="icon fa-twitter"><span class="label">Twitter</span></a>
							</li> -->
						</ul>
					</section>
					<!-- Copyright -->
					<div class="copyright">
						<ul class="menu">
							<li>&copy; SophieBoyer. All rights reserved.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
HTML;
echo $html;
}
 }

?>
 <?php
 function generate_menu(){
	if (!isset($_SESSION)) { session_start(); }
	if($_SESSION["language"]=='en-US'){
		$html = <<<HTML
	    <!-- Nav -->
			<nav id="nav">
				<ul>
					<li><a href="../../index.php">Home</a></li>
					<li>
						<a href="../../portfolio.php">Portfolio</a>
						
					</li>
					<li><a href="../../cv.php">Resume</a></li>
					<li>
						<img class="language" id="fr-FR" src="../../images/languages/icons8-france-96.png" height="20" alt="French" />
					</li>
				</ul>
			</nav>
HTML;
  echo $html;
	}
	elseif($_SESSION["language"]=='fr-FR'){
		$html = <<<HTML
	    <!-- Nav -->
			<nav id="nav">
				<ul>
					<li><a href="../../index.php">Accueil</a></li>
					<li>
						<a href="../../portfolio.php">Portfolio</a>
						
					</li>
					<li><a href="../../cv.php">Curriculum Vitae</a></li>
					<li>
					<img class="language" id="en-US" src="../../images/languages/icons8-etats-unis-96.png" height="20" alt="English" />
				</li>
				</ul>

			</nav>
HTML;
  echo $html;
	}
  
}
?>